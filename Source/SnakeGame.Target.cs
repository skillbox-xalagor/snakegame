// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class SnakeGameTarget : TargetRules
{
	public SnakeGameTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange(new[] { "SnakeGame" });
	}
}